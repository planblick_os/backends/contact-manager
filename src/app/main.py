from flask import Flask, jsonify, request, Response, stream_with_context
import json

from models.contacts import Contacts
from models.api_calls import ApiCalls

app = Flask(__name__)


@app.route('/contacts/fieldconfig', methods=["GET"])
def contacts_fieldconfig():
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    contact_config = Contacts().get_config_for_consumer(consumer_id)
    if contact_config is not None:
        result = json.loads(contact_config.data)
        return jsonify({"data": result})
    else:
        f = open('./ressources/default_fieldconfig.json')
        data = json.load(f)
        f.close()
        return jsonify(data)


@app.route('/contacts/list', methods=["GET"])
def contacts_list():
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    contact_list = Contacts().get_list_by_consumer(consumer_id)
    apikey = request.headers.get("apikey") if request.headers.get("apikey") is not None else request.args.get("apikey")
    users_group_memberships = ApiCalls().getGroupMembershipsByApikey(apikey)
    login = ApiCalls().getLoginByApiKey(apikey)

    def generate():
        yield "["
        first = True
        for data in contact_list:
            if data.data_owners != "":
                data_owners = json.loads(data.data_owners)
            else:
                data_owners = []
            matches = [match for match in users_group_memberships if match in data_owners]
            if f"user.{login}" in data_owners:
                matches.append(f"user.{login}")
            
            if len(matches) == 0 and len(data_owners) > 0:
                continue

            if not first:
                yield ","
            else:
                first = False
            yield (json.dumps({
                "contact_id": data.contact_id,
                "data": json.loads(data.data),
                "external_id": data.external_id,
                "data_owners": matches,
                "parent_ids": json.loads(data.parent_ids),
                "child_ids": json.loads(data.child_ids)
            }))

        yield "]"

    response = Response(stream_with_context(generate()), mimetype='text/json')
    return response


@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.before_request
def before_request():
    payload = request.get_json(silent=True)
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)
