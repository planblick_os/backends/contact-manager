from config import Config
from db.contacts import Contacts as DBContacts
from db.contact_configuration import Contact_Configuration as DBContact_Confirmation

class ContactsNotFound(Exception):
     pass

class Contacts():
    def __init__(self):
        pass

    def get_config_for_consumer(self, consumer_id):
        results = Config.db_session.query(DBContact_Confirmation) \
            .filter(DBContact_Confirmation.consumer_id == consumer_id) \
            .one_or_none()
        # Config.db_session.commit()
        return results

    def get_list_by_consumer(self, consumer_id):
        results = Config.db_session.query(DBContacts) \
            .filter(DBContacts.consumer_id == consumer_id) \
            .all()
        #Config.db_session.commit()

        return results



