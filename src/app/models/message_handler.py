import requests
import json
from time import sleep
from pbglobal.pblib.amqp import client
from config import Config
import traceback

from pbglobal.events.newContactCreated import newContactCreated
from pbglobal.events.contactUpdated import contactUpdated
from pbglobal.events.contactDeleted import contactDeleted
from pbglobal.events.contactConfigurationUpdated import contactConfigurationUpdated
from pbglobal.events.contactDataOwnerAdded import contactDataOwnerAdded
from pbglobal.events.contactDataOwnerRemoved import contactDataOwnerRemoved

from db.contacts import Contacts
from db.contact_configuration import Contact_Configuration
from pbglobal.pblib.helpers import list_diff

class message_handler():
    kong_api = "http://kong.planblick.svc:8001"
    queue_client = client()

    def handle_message(ch, method=None, properties=None, body=None):
        try:
            print("---------- Start handling Message ----------")
            event = method.routing_key
            body = body.decode("utf-8")
            instance = message_handler()
            if (event is not None):
                if method.routing_key == "createNewContact":
                    payload = json.loads(body)
                    new_contact_event = newContactCreated(skip_validation=True).from_json(newContactCreated, payload)
                    new_contact_event.publish()
                    ch.basic_ack(method.delivery_tag)

                elif method.routing_key == "updateContact":
                    payload = json.loads(body)
                    update_contact_event = contactUpdated(skip_validation=True).from_json(contactUpdated, payload)
                    update_contact_event.publish()
                    ch.basic_ack(method.delivery_tag)

                elif method.routing_key == "deleteContact":
                    payload = json.loads(body)
                    delete_contact_event = contactDeleted(skip_validation=True).from_json(contactDeleted, payload)
                    delete_contact_event.publish()
                    ch.basic_ack(method.delivery_tag)

                elif method.routing_key == "updateContactConfiguration":
                    payload = json.loads(body)
                    update_contact_configuration_event = contactConfigurationUpdated(skip_validation=True).from_json(contactConfigurationUpdated, payload)
                    update_contact_configuration_event.publish()
                    ch.basic_ack(method.delivery_tag)

                elif method.routing_key == "newContactCreated":
                    if instance.newContactCreated(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")

                elif method.routing_key == "contactUpdated":
                    if instance.contactUpdated(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")

                elif method.routing_key == "contactDeleted":
                    if instance.contactDeleted(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")

                elif method.routing_key == "contactConfigurationUpdated":
                    if instance.contactConfigurationUpdated(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")

                elif method.routing_key == "contactAddDataOwner":
                    if instance.contactAddDataOwner(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")

                elif method.routing_key == "contactDataOwnerAdded":
                    if instance.contactDataOwnerAdded(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")
                elif method.routing_key == "contactRemoveDataOwner":
                    if instance.contactRemoveDataOwner(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")

                elif method.routing_key == "contactDataOwnerRemoved":
                    if instance.contactDataOwnerRemoved(body):
                        ch.basic_ack(method.delivery_tag)
                        print("->Done")
                    else:
                        ch.basic_nack(method.delivery_tag)
                        sleep(3)
                        print("->Failed")
                else:
                    raise Exception("No handler for this  message or even not responsible: '" + event + "'")

        except Exception as e:
            print(e)
            traceback.print_exc()
            ch.basic_nack(method.delivery_tag)
            sleep(15)

        # authorization.dumpRedis()
        print("---------- End handling Message ----------")

    def contactAddDataOwner(self, msg):
        print("Handling contactAddDataOwner")
        command_payload = json.loads(msg)
        event = contactDataOwnerAdded.from_json(contactDataOwnerAdded, command_payload)
        event.publish()
        return True

    def contactDataOwnerAdded(self, msg):
        try:
            print("Handling accountDataOwnerAddedToRole")
            json_data = json.loads(msg)
            print(json_data)
            new_owners = json_data.get("data", {}).get("owners")

            contact = Config.db_session.query(Contacts).filter(
                Contacts.contact_id == json_data.get("data", {}).get("contact_id")).one_or_none()

            if contact is not None:
                current_owners = json.loads(contact.data_owners)
                merged_owners = current_owners + [owner for owner in new_owners if owner not in current_owners]

                contact.data_owners = json.dumps(merged_owners)
                contact.save()

            message = {"command": "refreshContacts",
                       "args": {"contact_id": contact.contact_id, "task_id": json_data.get("correlation_id")}}

            payload = {"room": contact.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def contactRemoveDataOwner(self, msg):
        print("Handling contactRemoveDataOwner")
        command_payload = json.loads(msg)
        event = contactDataOwnerRemoved.from_json(contactDataOwnerRemoved, command_payload)
        event.publish()
        return True

    def contactDataOwnerRemoved(self, msg):
        try:
            print("Handling accountDataOwnerAddedToRole")
            json_data = json.loads(msg)
            print(json_data)
            new_owners = json_data.get("data", {}).get("owners")

            contact = Config.db_session.query(Contacts).filter(
                Contacts.contact_id == json_data.get("data", {}).get("contact_id")).one_or_none()

            if contact is not None:
                current_owners = json.loads(contact.data_owners)
                merged_owners = [owner for owner in current_owners if owner not in new_owners]
                print("NEW OWNERS", current_owners, new_owners, merged_owners)
                contact.data_owners = json.dumps(merged_owners)
                contact.save()

            message = {"command": "refreshContacts",
                       "args": {"contact_id": contact.contact_id, "task_id": json_data.get("correlation_id")}}

            payload = {"room": contact.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def contactConfigurationUpdated(self, msg):
        try:
            json_data = json.loads(msg)
            contact_configuration_updated_event = contactConfigurationUpdated.from_json(contactConfigurationUpdated, json_data)

            #.filter(Contact_Configuration.contact_configuration_id == contact_configuration_updated_event.contact_configuration_id) \
            contact_configuration = Config.db_session.query(Contact_Configuration) \
                .filter(Contact_Configuration.consumer_id == contact_configuration_updated_event.consumer_id) \
                .first()

            if not contact_configuration:
                contact_configuration = Contact_Configuration()


            contact_configuration.consumer_id = contact_configuration_updated_event.consumer_id
            contact_configuration.contact_configuration_id = contact_configuration_updated_event.consumer_id
            contact_configuration.data = json.dumps(contact_configuration_updated_event.data)

            contact_configuration.save()

        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        message = {"command": "refreshContactConfiguration",
                   "args": {}}

        payload = {"room": contact_configuration.consumer_id, "data": message}
        client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

        return True

    def newContactCreated(self, msg):
        try:
            print("Handling newContactCreated")
            json_data = json.loads(msg)
            print(json_data)
            new_contact = newContactCreated.from_json(newContactCreated, json_data)

            Config.db_session.query(Contacts)\
                .filter(Contacts.consumer_id == new_contact.consumer_id)\
                .filter(Contacts.external_id == new_contact.external_id)\
                .delete()

            Config.db_session.query(Contacts)\
                .filter(Contacts.consumer_id == new_contact.consumer_id)\
                .filter(Contacts.contact_id == new_contact.contact_id)\
                .delete()

            contact = Contacts()
            contact.contact_id = new_contact.contact_id
            contact.consumer_id = new_contact.consumer_id
            contact.owner_id = new_contact.owner_id
            contact.data = json.dumps(new_contact.data)
            contact.external_id = new_contact.external_id if new_contact.external_id is not None else new_contact.contact_id
            contact.data_owners = json.dumps(new_contact.data_owners) if new_contact.data_owners is not None else "[]"
            if new_contact.parent_ids is not None:
                contact.parent_ids = json.dumps(new_contact.parent_ids)
                for parent in new_contact.parent_ids:
                    parentDBEntry = Config.db_session.query(Contacts) \
                        .filter(Contacts.consumer_id == new_contact.consumer_id) \
                        .filter(Contacts.contact_id == parent) \
                        .first()
                    if parentDBEntry is not None:
                        updateParentEvent = contactUpdated(skip_validation=True)
                        updateParentEvent.consumer_id = new_contact.consumer_id
                        updateParentEvent.owner_id = new_contact.consumer_id
                        updateParentEvent.owner = new_contact.consumer_id
                        updateParentEvent.creator = new_contact.creator
                        updateParentEvent.correlation_id = new_contact.correlation_id
                        updateParentEvent.contact_id = parent
                        child_ids = json.loads(parentDBEntry.child_ids)
                        if contact.contact_id not in child_ids:
                            child_ids.append(contact.contact_id)
                            updateParentEvent.child_ids = child_ids
                            updateParentEvent.publish()
            else:
                contact.parent_ids = "[]"

            if new_contact.child_ids is not None:
                contact.child_ids = json.dumps(new_contact.child_ids)
                for child in new_contact.child_ids:
                    childDBEntry = Config.db_session.query(Contacts) \
                        .filter(Contacts.consumer_id == new_contact.consumer_id) \
                        .filter(Contacts.contact_id == child) \
                        .first()
                    if childDBEntry is not None:
                        updateChildEvent = contactUpdated(skip_validation=True)
                        updateChildEvent.consumer_id = new_contact.consumer_id
                        updateChildEvent.owner_id = new_contact.consumer_id
                        updateChildEvent.owner = new_contact.consumer_id
                        updateChildEvent.creator = new_contact.creator
                        updateChildEvent.correlation_id = new_contact.correlation_id
                        updateChildEvent.contact_id = child
                        parent_ids = json.loads(childDBEntry.parent_ids)
                        if contact.contact_id not in parent_ids:
                            parent_ids.append(contact.contact_id)
                            updateChildEvent.parent_ids = parent_ids
                            updateChildEvent.publish()
            else:
                contact.child_ids = "[]"

            contact.save()

            message = {"command": "refreshContacts",
                       "args": {"contact_id": contact.contact_id, "task_id": json_data.get("correlation_id")}}

            payload = {"room": contact.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def contactUpdated(self, msg):
        try:
            print("Handling contactUpdated")
            json_data = json.loads(msg)
            contact_updated_event = contactUpdated.from_json(contactUpdated, json_data)

            contact = Config.db_session.query(Contacts)\
                .filter(Contacts.consumer_id == contact_updated_event.consumer_id)\
                .filter(Contacts.contact_id == contact_updated_event.contact_id)\
                .first()

            current_data_owners = json.loads(contact.data_owners)
            if not contact:
                contact = Config.db_session.query(Contacts)\
                    .filter(Contacts.consumer_id == contact_updated_event.consumer_id)\
                    .filter(Contacts.external_id == contact_updated_event.external_id)\
                    .first()


            if not contact:
                raise Exception("Contact not found")

            contact.consumer_id = contact_updated_event.consumer_id if contact_updated_event.consumer_id is not None else contact.consumer_id
            contact.owner_id = contact_updated_event.owner_id if contact_updated_event.owner_id is not None else contact.owner_id
            contact.data = json.dumps(contact_updated_event.data) if contact_updated_event.data is not None else contact.data
            contact.external_id = contact_updated_event.external_id if contact_updated_event.external_id is not None else contact.contact_id

            if contact_updated_event.parent_ids is not None:
                removed_parents = list_diff(json.loads(contact.parent_ids), contact_updated_event.parent_ids)
                all_parent_ids = [*json.loads(contact.parent_ids), *contact_updated_event.parent_ids]
                contact.parent_ids = json.dumps(contact_updated_event.parent_ids)

                for parent in all_parent_ids:
                    parentDBEntry = Config.db_session.query(Contacts) \
                        .filter(Contacts.consumer_id == contact_updated_event.consumer_id) \
                        .filter(Contacts.contact_id == parent) \
                        .first()

                    if parentDBEntry is not None:
                        updateParentEvent = contactUpdated(skip_validation=True)
                        updateParentEvent.consumer_id = contact_updated_event.consumer_id
                        updateParentEvent.owner_id = contact_updated_event.consumer_id
                        updateParentEvent.owner = contact_updated_event.consumer_id
                        updateParentEvent.creator = contact_updated_event.creator
                        updateParentEvent.correlation_id = contact_updated_event.correlation_id
                        updateParentEvent.contact_id = parent
                        child_ids = json.loads(parentDBEntry.child_ids)

                        if contact.contact_id in child_ids:
                            child_ids.remove(contact.contact_id)

                        child_ids.append(contact.contact_id)

                        if parent in removed_parents:
                            child_ids.remove(contact.contact_id)

                        updateParentEvent.child_ids = child_ids
                        updateParentEvent.parent_ids = json.loads(parentDBEntry.parent_ids)

                        if json.loads(parentDBEntry.child_ids) != child_ids:
                            updateParentEvent.publish()
            else:
                contact.parent_ids = "[]"

            if contact_updated_event.child_ids is not None:
                removed_childs = list_diff(json.loads(contact.child_ids), contact_updated_event.child_ids)
                all_child_ids = [*json.loads(contact.child_ids), *contact_updated_event.child_ids]

                contact.child_ids = json.dumps(contact_updated_event.child_ids)
                for child in all_child_ids:
                    childDBEntry = Config.db_session.query(Contacts) \
                        .filter(Contacts.consumer_id == contact_updated_event.consumer_id) \
                        .filter(Contacts.contact_id == child) \
                        .first()
                    if childDBEntry is not None:
                        updateChildsEvent = contactUpdated(skip_validation=True)
                        updateChildsEvent.consumer_id = contact_updated_event.consumer_id
                        updateChildsEvent.owner_id = contact_updated_event.consumer_id
                        updateChildsEvent.owner = contact_updated_event.consumer_id
                        updateChildsEvent.creator = contact_updated_event.creator
                        updateChildsEvent.correlation_id = contact_updated_event.correlation_id
                        updateChildsEvent.contact_id = child
                        parent_ids = json.loads(childDBEntry.parent_ids)

                        if contact.contact_id in parent_ids:
                            parent_ids.remove(contact.contact_id)

                        parent_ids.append(contact.contact_id)

                        if child in removed_childs:
                            parent_ids.remove(contact.contact_id)

                        updateChildsEvent.parent_ids = parent_ids
                        updateChildsEvent.child_ids = json.loads(childDBEntry.child_ids)

                        if json.loads(childDBEntry.parent_ids) != parent_ids:
                            updateChildsEvent.publish()
            else:
                contact.child_ids = "[]"

            current_data_owners = current_data_owners
            merged_owners = current_data_owners + [owner for owner in json_data.get("data_owners", []) if
                                                   owner not in current_data_owners]
            contact.data_owners = json.dumps(merged_owners)
            contact.save()


        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()

            if "Contact not found" in e.__repr__():
                message = {"command": "showAlert",
                           "args": {"text": "Contact could not be found"}}

                payload = {"room": contact_updated_event.consumer_id, "data": message}
                client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
                return True
            else:
                return False

        message = {"command": "refreshContacts",
                   "args": {"contact_id": contact.contact_id, "task_id": json_data.get("correlation_id")}}

        payload = {"room": contact.consumer_id, "data": message}
        client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

        return True

    def contactDeleted(self, msg):
        try:
            print("Handling contactDeleted")
            json_data = json.loads(msg)
            print(json_data)
            contact_deleted_event = contactDeleted.from_json(contactDeleted, json_data)

            deleted = Config.db_session.query(Contacts) \
                .filter(Contacts.consumer_id == contact_deleted_event.consumer_id) \
                .filter(Contacts.contact_id == contact_deleted_event.contact_id) \
                .first()

            childs = json.loads(deleted.child_ids)
            for child in childs:
                childDBEntry = Config.db_session.query(Contacts) \
                    .filter(Contacts.consumer_id == deleted.consumer_id) \
                    .filter(Contacts.contact_id == child) \
                    .first()

                if childDBEntry is not None:
                    updateChildEvent = contactUpdated(skip_validation=True)
                    updateChildEvent.consumer_id = contact_deleted_event.consumer_id
                    updateChildEvent.owner_id = contact_deleted_event.consumer_id
                    updateChildEvent.owner = contact_deleted_event.consumer_id
                    updateChildEvent.creator = contact_deleted_event.creator
                    updateChildEvent.correlation_id = contact_deleted_event.correlation_id
                    updateChildEvent.contact_id = child
                    parent_ids = json.loads(childDBEntry.parent_ids)
                    if deleted.contact_id in parent_ids:
                        parent_ids.remove(deleted.contact_id)
                        updateChildEvent.parent_ids = parent_ids
                        updateChildEvent.publish()

            parents = json.loads(deleted.parent_ids)
            for parent in parents:
                parentDBEntry = Config.db_session.query(Contacts) \
                    .filter(Contacts.consumer_id == deleted.consumer_id) \
                    .filter(Contacts.contact_id == parent) \
                    .first()

                if parentDBEntry is not None:
                    updateParentEvent = contactUpdated(skip_validation=True)
                    updateParentEvent.consumer_id = contact_deleted_event.consumer_id
                    updateParentEvent.owner_id = contact_deleted_event.consumer_id
                    updateParentEvent.owner = contact_deleted_event.consumer_id
                    updateParentEvent.creator = contact_deleted_event.creator
                    updateParentEvent.correlation_id = contact_deleted_event.correlation_id
                    updateParentEvent.contact_id = parent
                    child_ids = json.loads(parentDBEntry.child_ids)
                    if deleted.contact_id in child_ids:
                        child_ids.remove(deleted.contact_id)
                        updateParentEvent.child_ids = child_ids
                        updateParentEvent.publish()

            Config.db_session.query(Contacts) \
                .filter(Contacts.consumer_id == contact_deleted_event.consumer_id) \
                .filter(Contacts.contact_id == contact_deleted_event.contact_id) \
                .delete()

            Config.db_session.query(Contacts) \
                .filter(Contacts.consumer_id == contact_deleted_event.consumer_id) \
                .filter(Contacts.external_id == contact_deleted_event.external_id) \
                .delete()

        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        message = {"command": "refreshContacts",
                   "args": {"task_id": json_data.get("correlation_id")}}

        payload = {"room": contact_deleted_event.consumer_id, "data": message}
        client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        return True




